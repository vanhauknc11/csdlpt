﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_FINAL
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();


        }

        private Form CheckExists(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
            {
                if (f.GetType() == ftype)
                {
                    return f;
                }

            }
            return null;


        }

        void status(bool flag)
        {
            barButtonItem1.Enabled = flag;
            barButtonItem2.Enabled = flag;
            barButtonItem3.Enabled = flag;
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmNhanVien));
            if (frm != null) frm.Activate();
            else
            {
                frmNhanVien f = new frmNhanVien();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmMonhoc));
            if (frm != null) frm.Activate();
            else
            {
                frmMonhoc f = new frmMonhoc();
                f.MdiParent = this;
                f.Show();
            }

        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmHocphi));
            if (frm != null) frm.Activate();
            else
            {
                frmHocphi f = new frmHocphi();
                f.MdiParent = this;
                f.Show();
            }

        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            if (Program.mGroup == "PKeToan")
            {
                status(false);
            }
        }
    }
}

      
