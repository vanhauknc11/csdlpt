﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_FINAL
{
    public partial class frmNhanVien : Form
    {
        public frmNhanVien()
        {
            InitializeComponent();

        }

        private void sINHVIENBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.sINHVIENBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dS);

        }
        public int cc = 0;

        void status(bool flag)
        {
            barButtonItem1.Enabled = flag;
        }

        private void frmNhanVien_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.SINHVIEN' table. You can move, or remove it, as needed.


            //  MessageBox.Show(cc.ToString()+"Load");
            if (Program.mGroup != "PGV")
            {
                comboBox1.Visible = false;
            }
            else
            {
                comboBox1.Visible = true;
                comboBox1.DataSource = Program.bds_dspm;
                comboBox1.DisplayMember = "TENCN";
                comboBox1.ValueMember = "TENSERVER";
                comboBox1.SelectedIndex = Program.mChinhanh;
            }
       
            
            // MessageBox.Show(khoa.ToString());
          //  dS.EnforceConstraints = false;
            this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connstr;
            this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
            try
            {
                Program.makhoa = ((DataRowView)sINHVIENBindingSource[0])["MALOP"].ToString();
              //  MessageBox.Show(Program.makhoa);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            cc = 2;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sINHVIENBindingSource.AddNew();
            mALOPTextEdit.Text = Program.makhoa;
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if(comboBox1.SelectedIndex != Program.mChinhanh)
            {
                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
                Program.servername = comboBox1.SelectedValue.ToString();
                if (cc == 2)
                {
                    if (Program.KetNoi() == 1) { 

                    this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                        try
                        {
                            Program.makhoa = ((DataRowView)sINHVIENBindingSource[0])["MALOP"].ToString();
                          //  MessageBox.Show(Program.makhoa);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Loi ket nnoi");
                    }
                }
            }
             if(comboBox1.SelectedIndex == Program.mChinhanh)
            {
                
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
                Program.servername = comboBox1.SelectedValue.ToString();
                if (cc == 2)
                {
                    
                    if (Program.KetNoi() == 1) { 
                    this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                        try { 
                            Program.makhoa  = ((DataRowView)sINHVIENBindingSource[0])["MALOP"].ToString();
                        //    MessageBox.Show(Program.makhoa);
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Loi ket nnoi");
                    }
                }
            }
         
            
            
            

            /*
            if (comboBox1.SelectedIndex != Program.mChinhanh)
            {
                    MessageBox.Show("change");
                    Program.mChinhanh = comboBox1.SelectedIndex;
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                    Program.servername = comboBox1.SelectedValue.ToString();
                     MessageBox.Show(Program.connstr+" "+Program.mChinhanh);
                    if (Program.KetNoi() == 1)
                    {
                    
                        this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                        String makhoa = ((DataRowView)sINHVIENBindingSource[0])["MALOP"].ToString();
                        MessageBox.Show(makhoa);
                    }

            }
            else
            {

            }
           
    */
            


        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            try {
                sINHVIENBindingSource.EndEdit();
                sINHVIENBindingSource.ResetCurrentItem();
                this.sINHVIENTableAdapter.Update(this.dS.SINHVIEN);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Co loi xay ra roi dcm : " + ex);
            }
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            sINHVIENBindingSource.CancelEdit();

        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
    }
}
