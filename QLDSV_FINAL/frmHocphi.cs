﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLDSV_FINAL
{
    public partial class frmHocphi : Form
    {
        public frmHocphi()
        {
            InitializeComponent();

        }


        private void frmHocphi_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.HOCPHI' table. You can move, or remove it, as needed.
           
            btnsearch.Enabled = false;
            btndelete.Enabled = false;
            btnsave.Enabled = false;
            btnadd.Enabled = false;
            btnupdate.Enabled = false;
        }





        private void mASVTextEdit_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void mASVLabel_Click(object sender, EventArgs e)
        {

        }


        
        private void barButtonItem2_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string mssv = hpmssv1.Text.Trim();
            string nienkhoa = hpnienkhoa.Text.Trim();
            string hocky = hphocky.Text.Trim();
            string hocphi = hphocphi.Text.Trim();
            string sotien = hpstdd.Text.Trim();

            int n = dataGridView1.Rows.Count;
          //  MessageBox.Show(n.ToString());
            
            for(int i = 0; i < n-1; i++)
            {
                string hk = dataGridView1.Rows[i].Cells["HOCKY"].Value.ToString().Trim();
              //  MessageBox.Show(hk);
                string nk = dataGridView1.Rows[i].Cells["NIENKHOA"].Value.ToString().Trim();
                if (String.Compare(nienkhoa, nk, true) == 0 && String.Compare(hocky, hk, true) == 0)
                {
                        MessageBox.Show("Sinh viên đã đóng học phí cho học kỳ này","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        return;
                }
            }
            
           
            string strLenh = "EXEC HOCPHIINSERT '"+mssv+"','"+nienkhoa+"','"+hocky+"','"+hocphi+"','"+sotien+"' ";

            Program.myReader = Program.ExecSqlDataReader(strLenh);
            if (Program.myReader == null) return;
            MessageBox.Show(Program.myReader.RecordsAffected.ToString());
            Program.myReader.Close();
        }

      
        


        void bonnut(bool flag)
        {
            btndelete.Enabled = flag;
            btnsave.Enabled = flag;
            btnadd.Enabled = flag;
            btnupdate.Enabled = flag;

            hpmssv1.ReadOnly = flag;
        }

        void tableRefresh(string mssv)
        {
            string strLenh = "EXEC SP_HOCPHI '" + mssv + "' ";

            Program.myReader = Program.ExecSqlDataReader(strLenh);

            if (Program.myReader == null) return;

            DataTable dt = new DataTable();
            try
            {
                dt.Load(Program.myReader);
                hphoten.Text = dt.Rows[0]["HOTEN"].ToString();
                hplop.Text = dt.Rows[0]["LOP"].ToString();

                dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = dt;


                // dataGridView1.ColumnCount = 4;
                dataGridView1.Columns[0].DataPropertyName = "NIENKHOA";
                dataGridView1.Columns[1].DataPropertyName = "HOCKY";
                dataGridView1.Columns[2].DataPropertyName = "HOCPHI";
                dataGridView1.Columns[3].DataPropertyName = "SOTIENDADONG";
                this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            catch
            {
                MessageBox.Show("Mã số sinh viên không tồn tại !", "Thông báo !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bonnut(false);
            }


            Program.myReader.Close();
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string mssv = hpmssv1.Text.Trim();

          

            if (mssv.Length < 4)
            {
                MessageBox.Show("Mã số sinh viên phải nhiều hơn 4 kí tự","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }

            bonnut(true);
            btnsave.Enabled = false;

            tableRefresh(mssv);




        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            hpmssv1.ReadOnly = false;
            btnsearch.Enabled = true;
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            hpnienkhoa.ReadOnly = false;
            hphocky.ReadOnly = false;
            hphocphi.ReadOnly = false;
            hpstdd.ReadOnly = false;
            btnsave.Enabled = true;


            hpnienkhoa.Text = "";
            hphocky.Text = "";
            hphocphi.Text = "";
            hpstdd.Text = "";
        }

        private void barButtonItem1_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            hpmssv1.ReadOnly = false;
        }

        private void barButtonItem4_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string mssv = hpmssv1.Text.Trim();
            string nienkhoa = hpnienkhoa.Text.Trim();
            string hocky = hphocky.Text.Trim();
            string hocphi = hphocphi.Text.Trim();
            string sotien = hpstdd.Text.Trim();

            string sql = "UPDATE HOCPHI SET HOCPHI = '" + hocphi + "', SOTIENDADONG = '" + sotien + "' WHERE MASV = '" + mssv + "' and HOCKY = '" + hocky + "' and NIENKHOA = '" + nienkhoa + "'";
            try { 
            Program.myReader = Program.ExecSqlDataReader(sql);
            if( Program.myReader.RecordsAffected == 1)
            {
                MessageBox.Show("Chỉnh sửa thành công !","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
            }
            else
            {
                MessageBox.Show("Chỉnh sửa thất bại ! Bạn không được sửa Niên Khóa và Học Kỳ ", "Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            Program.myReader.Close();

            tableRefresh(mssv);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Bạn không được sửa Niên Khóa và Học Kỳ "+ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void barButtonItem5_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string mssv = hpmssv1.Text.Trim();
            string nienkhoa = hpnienkhoa.Text.Trim();
            string hocky = hphocky.Text.Trim();

            string sql = "DELETE FROM HOCPHI WHERE MASV='"+mssv+"' and NIENKHOA='"+nienkhoa+"' and HOCKY='"+hocky+"'";

            DialogResult dialog = MessageBox.Show("Bạn có chắc chắn muốn xóa ?", "Thông Báo", MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if (dialog == DialogResult.Yes)
            {
                try {
                Program.myReader = Program.ExecSqlDataReader(sql);
                if (Program.myReader.RecordsAffected == 1)
                {
                    MessageBox.Show("Xóa thành công !","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Xóa thất bại ! ","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                Program.myReader.Close();

                tableRefresh(mssv);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Xóa thất bại ! Đã có lỗi xảy ra "+ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            
        }

        private void barButtonItem6_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string mssv = hpmssv1.Text.Trim();
            string nienkhoa = hpnienkhoa.Text.Trim();
            string hocky = hphocky.Text.Trim();
            string hocphi = hphocphi.Text.Trim();
            string sotien = hpstdd.Text.Trim();
         
           
            if (mssv.Length==0 ||nienkhoa.Length == 0 || hocky.Length == 0 || hocphi.Length == 0 || sotien.Length == 0)
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            int n = dataGridView1.Rows.Count;
            //  MessageBox.Show(n.ToString());

            for (int i = 0; i < n - 1; i++)
            {
                string hk = dataGridView1.Rows[i].Cells["HOCKY"].Value.ToString().Trim();
                //  MessageBox.Show(hk);
                string nk = dataGridView1.Rows[i].Cells["NIENKHOA"].Value.ToString().Trim();
                if (String.Compare(nienkhoa, nk, true) == 0 && String.Compare(hocky, hk, true) == 0)
                {
                    MessageBox.Show("Sinh viên đã đóng học phí cho học kỳ này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }



            string strLenh = "EXEC HOCPHIINSERT '" + mssv + "','" + nienkhoa + "','" + hocky + "','" + hocphi + "','" + sotien + "' ";
            try {
                Program.myReader = Program.ExecSqlDataReader(strLenh);
                if (Program.myReader == null) return;
                //   MessageBox.Show(Program.myReader.RecordsAffected.ToString());
                if (Program.myReader.RecordsAffected == 1)
                {
                    btnsave.Enabled = false;
                    MessageBox.Show("Thêm mới thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                }
                else
                {
                    MessageBox.Show("Thêm mới thất bại !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                Program.myReader.Close();
                tableRefresh(mssv);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Thêm mới thất bại !"+ex.ToString(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                dataGridView1.CurrentRow.Selected = true;
                string nienkhoa = dataGridView1.Rows[e.RowIndex].Cells["NIENKHOA"].FormattedValue.ToString();
                string hocky = dataGridView1.Rows[e.RowIndex].Cells["HOCKY"].FormattedValue.ToString();
                string hocphi = dataGridView1.Rows[e.RowIndex].Cells["HOCPHI"].FormattedValue.ToString();
                string stdd = dataGridView1.Rows[e.RowIndex].Cells["SOTIENDADONG"].FormattedValue.ToString();

                hpnienkhoa.Text = nienkhoa;
                hphocky.Text = hocky;
                hphocphi.Text = hocphi;
                hpstdd.Text = stdd;
            }
        }

        private void btnexit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}
